;;; mu4e-static.el --- Reply from the to address     -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Ian Eure

;; Author: Ian Eure <ian@retrospec.tv>
;; Keywords: mail

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(defun mu4e-static--matching-address (&optional pred)
  (let ((pred (or pred #'mu4e-user-mail-address-p)))
    (thread-first
      (lambda (adata) (cl-destructuring-bind (user . addr) adata (funcall pred addr)))
      (cl-remove-if-not (mu4e-message-field mu4e-compose-parent-message :to))
      car)))

(defun mu4e-static-compose-hook ()
  "Use To: address from original message as From: in reply.

But only of the original message was sent to an email in MU4E-USER-MAIL-ADDRESS-LIST."
  (when mu4e-compose-parent-message
    (save-excursion
      (when-let ((dest (mu4e-static--matching-address)))
        (cl-destructuring-bind (from-user . from-addr) dest
          (message-position-on-field "From")
          (message-beginning-of-line)
          (delete-region (point) (line-end-position))
          (insert (format "%s <%s>" (or from-user user-full-name) from-addr)))))))

(provide 'mu4e-static)
;;; mu4e-static.el ends here
